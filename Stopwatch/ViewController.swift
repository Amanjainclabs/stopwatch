import UIKit

class ViewController: UIViewController {
    
    /* 
       Created 4 variables of type NSTimer generate
       delays of ms, sec, min and hour.
    */
    var timerSEC = NSTimer()
    var timerMIN = NSTimer()
    var timerMS = NSTimer()
    var timerHOUR = NSTimer()
    
    // label outlets to display the time
    @IBOutlet weak var timeMS: UILabel!
    @IBOutlet weak var timeSEC: UILabel!
    @IBOutlet weak var timeMIN: UILabel!
    @IBOutlet weak var timeHOUR: UILabel!
    @IBOutlet weak var splitTimer: UITextView!
    
    // various count variables to update the time.
    var countMS = 0
    var countSEC = 0
    var countMIN = 0
    var countHOUR = 0
    var countSplit = 0
    
    /*
        instructions executed when play button is pressed
        4 statements which are given time intervals so that 
        time is calculated under 4 constraints.
    */
    @IBAction func play(sender : AnyObject) {
        
        timerMS = NSTimer.scheduledTimerWithTimeInterval(0.001, target: self, selector: Selector("resultMS"), userInfo: nil, repeats: true)
        
        timerSEC = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("resultSEC"), userInfo: nil, repeats: true)
        
        timerMIN = NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: Selector("resultMIN"), userInfo: nil, repeats: true)
        
        timerHOUR = NSTimer.scheduledTimerWithTimeInterval(3600, target: self, selector: Selector("resultHOUR"), userInfo: nil, repeats: true)
        
    }
    
    // Displays split time stored
    @IBAction func splitTime(sender: AnyObject) {
        
        if countMS != 0 {
            
        var recordTime = timeHOUR.text!+" : "+timeMIN.text!+" : "+timeSEC.text!+" : "+timeMS.text!
        splitTimer.text = splitTimer.text+"\n"+String(++countSplit)+". "+recordTime
        }
        
        
    }
    
    // clear the recorded time
    @IBAction func clearAll(sender: AnyObject) {
        
        splitTimer.text = ""
    }
    
    // stop the timer when pause button is pressed.
    @IBAction func pause(sender : AnyObject) {
        
        // .invalidate stops the running of NSTimer.
        timerMS.invalidate()
        timerSEC.invalidate()
        timerMIN.invalidate()
        timerHOUR.invalidate()
    }
    
    //  reset the timer.
    @IBAction func reset(sender : AnyObject) {
        
        // reset the counters to 0.
        countMS = 0
        countSEC = 0
        countMIN = 0
        countHOUR = 0
        
        // change the values of labels to 0.
        timeMS.text = "000"
        timeSEC.text = "00"
        timeMIN.text = "00"
        timeHOUR.text = "00"
        
        // stop the timer execution.
        timerMS.invalidate()
        timerSEC.invalidate()
        timerMIN.invalidate()
        timerHOUR.invalidate()
    }

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // function called every ms.
    func resultMS() {
        
        // update the counter and display in label.
        countMS++
        
        // reset ms when counter reaches 1000.
        if (countMS == 1000){
            
            countMS = 0
        }
        
        timeMS.text = String(countMS)
    }
    
    // function called every sec.
    func resultSEC() {
        
        // update the counter and display in label.
        countSEC++
        
        // reset sec when counter reaches 60.
        if (countSEC == 60){
            
            countSEC = 0
        }
        if countSEC / 10 == 0 {
            
           timeSEC.text = "0"+String(countSEC)
            
        }else{
            
            timeSEC.text = String(countSEC)
        }
    }
    
    // function called every min
    func resultMIN() {
        
        // update the counter and display in label.
        countMIN++
        
        // reset min when counter reaches 60.
        if (countMIN == 60){
            
            countMIN = 0
        }
        if countMIN / 10 == 0 {
            
            timeMIN.text = "0"+String(countMIN)
            
        }else{
            
            timeMIN.text = String(countMIN)
        }
        
    }
    
    // function called every hour.
    func resultHOUR() {
        
        // update the counter and display in label.
        countHOUR++
        
        if countHOUR / 10 == 0 {
            
            timeHOUR.text = "0"+String(countHOUR)
            
        }else{
            
            timeHOUR.text = String(countHOUR)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
}

